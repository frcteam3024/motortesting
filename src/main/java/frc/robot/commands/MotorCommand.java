package frc.robot.commands;

import static java.lang.Math.abs;
import static frc.robot.Constants.OIConstants.*;

import java.util.function.Supplier;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem;

public class MotorCommand extends CommandBase {

  private DriveSubsystem driveSubsystem;
  private Supplier<Double> leftAxisFunction;

  public MotorCommand(DriveSubsystem driveSubsystem, Supplier<Double> leftAxisFunction) {
    this.driveSubsystem = driveSubsystem;
    this.leftAxisFunction = leftAxisFunction;
    addRequirements(driveSubsystem);
  }

  @Override
  public void initialize() {
    driveSubsystem.brakeMode();
  }

  @Override
  public void execute() {
    double driverLeftAxis = leftAxisFunction.get();
    double motorOutputs = driverLeftAxis * 1;

    if (abs(motorOutputs) < MIN_MOTOR_OUTPUT) motorOutputs  = 0;
    
    SmartDashboard.putNumber("motorOut", motorOutputs);
    driveSubsystem.setOutput(motorOutputs);
  }

  @Override
  public void end(boolean interrupted) {
    double zeroSpeed = 0;
    driveSubsystem.setOutput(zeroSpeed);
    driveSubsystem.coastMode();
  }

  @Override
  public boolean isFinished() {
    return false;
  }
}
