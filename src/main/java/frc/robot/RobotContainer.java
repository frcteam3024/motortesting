package frc.robot;

import static frc.robot.Constants.OIConstants.*;

import java.security.InvalidKeyException;

import edu.wpi.first.wpilibj.Joystick;
import frc.robot.commands.MotorCommand;
import frc.robot.subsystems.DriveSubsystem;

public class RobotContainer {
  
  private DriveSubsystem driveSubsystem;

  private final Joystick copilotController;

  public RobotContainer() {

    copilotController = new Joystick(COPILOT_JOYSTICK_PORT);

    try {
      driveSubsystem = new DriveSubsystem();
    } catch (InvalidKeyException e) {
      e.printStackTrace();
      driveSubsystem = null;
    }

    MotorCommand motorCommand = new MotorCommand(
        driveSubsystem,
        () -> copilotController.getRawAxis(COPILOT_LEFT_Y_AXIS)
    );
    driveSubsystem.setDefaultCommand(motorCommand);

  }
  
}