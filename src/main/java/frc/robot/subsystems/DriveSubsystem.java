package frc.robot.subsystems;

import static frc.robot.Constants.DriveConstants.*;

import java.security.InvalidKeyException;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class DriveSubsystem extends SubsystemBase {
  
  private final TalonSRX talon    = new TalonSRX(ID);
  private final VictorSPX victor  = new VictorSPX(ID);
  private final CANSparkMax spark = new CANSparkMax(ID, MotorType.kBrushless);

  public DriveSubsystem() throws InvalidKeyException {
    switch (MOTOR_TYPE) {
      case CAN_SPARK_MAX:
        spark.setInverted(INVERTED);
        break;
      case TALON_SRX:
        talon.setInverted(INVERTED);
        break;
      case VICTOR_SPX:
        victor.setInverted(INVERTED);
        break;
      default:
        throw new InvalidKeyException("invalid motor type: " + MOTOR_TYPE);
    }
  }

  public void setOutput(double speed) {
    switch (MOTOR_TYPE) {
      case CAN_SPARK_MAX:
        spark.set(speed);
        break;
      case TALON_SRX:
        talon.set(ControlMode.PercentOutput, speed);
        break;
      case VICTOR_SPX:
        victor.set(ControlMode.PercentOutput, speed);
        break;
    }
  }

  public void brakeMode() {
    switch (MOTOR_TYPE) {
      case CAN_SPARK_MAX:
        spark.setIdleMode(IdleMode.kBrake);
        break;
      case TALON_SRX:
        talon.setNeutralMode(NeutralMode.Brake);
        break;
      case VICTOR_SPX:
        victor.setNeutralMode(NeutralMode.Brake);
        break;
    }
  }

  public void coastMode() {
    switch (MOTOR_TYPE) {
      case CAN_SPARK_MAX:
        spark.setIdleMode(IdleMode.kCoast);
        break;
      case TALON_SRX:
        talon.setNeutralMode(NeutralMode.Coast);
        break;
      case VICTOR_SPX:
        victor.setNeutralMode(NeutralMode.Coast);
        break;
    }
  }
}
